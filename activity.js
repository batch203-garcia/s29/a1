db.users.find({
    $or: [{
            firstName: { $regex: 'S', $options: '$i' }
        },
        {
            lastName: { $regex: 'D', $options: '$i' }
        }
    ]
}, {
    firstName: 1,
    lastName: 1,
    _id: 0
});

db.users.find({
    $and: [
        { department: "HR" },
        { age: { $gte: 70 } }
    ]
});

db.users.find({
    $and: [
        { firstName: { $regex: 'E', $options: '$i' } },
        { age: { $lte: 30 } }
    ]
});